description: build fix from (skia) upstream -
	upstream used freetype 2.11.1 API stuff, realized everyone was
	still on 2.11.0, and added this workaround to support older ft.
        Unfortunately not in time for the chromium 96 release, though.
	This comes from Skia upstream:
	https://github.com/google/skia/commit/b65f0fa084c47e1ea688d61a65d0fc8dfdf9eb03
	Drop this once either chromium updates skia or debian updates
	ft to 2.11.1.


Avoid using FreeType 2.11.0 COLRv1 headers

Skia relies on the existence of FT_COLR_COMPOSITE_PLUS, FT_ClipBox, and
FT_Get_Color_Glyph_ClipBox for COLRv1 support. However, these were not
available in the released FreeType 2.11.0 headers since the
specification later changed to require these. In addition, since 2.11.0,
FT_PaintRadialGradient changed the types of two of its members and the
Skia code must depend on this change.

Unrelated, the FT_STATIC_CAST macro was added after these changes. So
undefine TT_SUPPORT_COLRV1 if FreeType is less than 2.11.1 unless
FT_STATIC_CAST is defined.

Change-Id: Id827de5a6500fad900f9d9f2a11b1c69cfbd6aa8
Reviewed-on: https://skia-review.googlesource.com/c/skia/+/475646
Reviewed-by: Herb Derby <herb@google.com>
Commit-Queue: Ben Wagner <bungeman@google.com>

--- a/third_party/skia/src/ports/SkFontHost_FreeType_common.cpp
+++ b/third_party/skia/src/ports/SkFontHost_FreeType_common.cpp
@@ -31,7 +31,20 @@
 #include FT_SYNTHESIS_H
 
 #ifdef TT_SUPPORT_COLRV1
-#include "src/core/SkScopeExit.h"
+// FT_ClipBox and FT_Get_Color_Glyph_ClipBox introduced VER-2-11-0-18-g47cf8ebf4
+// FT_COLR_COMPOSITE_PLUS and renumbering introduced VER-2-11-0-21-ge40ae7569
+// FT_SIZEOF_LONG_LONG introduced VER-2-11-0-31-gffdac8d67
+// FT_PaintRadialGradient changed size and layout at VER-2-11-0-147-gd3d3ff76d
+// FT_STATIC_CAST introduced VER-2-11-0-172-g9079c5d91
+// So undefine TT_SUPPORT_COLRV1 before 2.11.1 but not if FT_STATIC_CAST is defined.
+#if (((FREETYPE_MAJOR)  < 2) || \
+     ((FREETYPE_MAJOR) == 2 && (FREETYPE_MINOR)  < 11) || \
+     ((FREETYPE_MAJOR) == 2 && (FREETYPE_MINOR) == 11 && (FREETYPE_PATCH) < 1)) && \
+    !defined(FT_STATIC_CAST)
+#    undef TT_SUPPORT_COLRV1
+#else
+#    include "src/core/SkScopeExit.h"
+#endif
 #endif
 
 // FT_LOAD_COLOR and the corresponding FT_Pixel_Mode::FT_PIXEL_MODE_BGRA
--- a/third_party/skia/src/ports/SkFontHost_FreeType.cpp
+++ b/third_party/skia/src/ports/SkFontHost_FreeType.cpp
@@ -67,6 +67,21 @@
 #  include <dlfcn.h>
 #endif
 
+#ifdef TT_SUPPORT_COLRV1
+// FT_ClipBox and FT_Get_Color_Glyph_ClipBox introduced VER-2-11-0-18-g47cf8ebf4
+// FT_COLR_COMPOSITE_PLUS and renumbering introduced VER-2-11-0-21-ge40ae7569
+// FT_SIZEOF_LONG_LONG introduced VER-2-11-0-31-gffdac8d67
+// FT_PaintRadialGradient changed size and layout at VER-2-11-0-147-gd3d3ff76d
+// FT_STATIC_CAST introduced VER-2-11-0-172-g9079c5d91
+// So undefine TT_SUPPORT_COLRV1 before 2.11.1 but not if FT_STATIC_CAST is defined.
+#if (((FREETYPE_MAJOR)  < 2) || \
+     ((FREETYPE_MAJOR) == 2 && (FREETYPE_MINOR)  < 11) || \
+     ((FREETYPE_MAJOR) == 2 && (FREETYPE_MINOR) == 11 && (FREETYPE_PATCH) < 1)) && \
+    !defined(FT_STATIC_CAST)
+#    undef TT_SUPPORT_COLRV1
+#endif
+#endif
+
 //#define ENABLE_GLYPH_SPEW     // for tracing calls
 //#define DUMP_STRIKE_CREATION
 //#define SK_FONTHOST_FREETYPE_RUNTIME_VERSION
